
package onlineshop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ShopController {
    
    private ProductFileManagement productFileManagement; 
    private Scanner scanner;
    public ProductList productList;
   
    public ShopController() throws IOException{
        productFileManagement = new ProductFileManagement();
        scanner = new Scanner(System.in);
        productList = new ProductList();   
    }
    
    public void addProduct() throws IOException{
        String productInput = "";
        
        while(true){
            ProductDetails prodDetails = new ProductDetails();
            
            System.out.println("Enter product id");
            productInput = scanner.nextLine();
            prodDetails.id = Integer.parseInt(productInput);
            
            System.out.println("Enter product name");           
            productInput = scanner.nextLine(); 
            prodDetails.name = productInput;
            
            System.out.println("Enter product category");           
            productInput = scanner.nextLine(); 
            prodDetails.category = productInput;
            
            System.out.println("Enter product price");           
            productInput = scanner.nextLine(); 
            prodDetails.price = Double.parseDouble(productInput);
            
            System.out.println("Enter product amount");           
            productInput = scanner.nextLine(); 
            prodDetails.amount = Integer.parseInt(productInput);
        
            productFileManagement.addProduct(prodDetails); 
            
            System.out.println("Enter q to exit");
            productInput = scanner.nextLine();
            
            if(productInput.equals("q")){
                break;
            }
        }
    }  
    
    public void showProducts() throws IOException{
        System.out.println("Product list: ");
        ProductList productList = productFileManagement.getProducts();
        
        for(int i=0;i<productList.product.size();i++){
             ProductDetails productDetails = productList.product.get(i);
             System.out.println("Product ID: " + productDetails.id);
             System.out.println("Product name: " + productDetails.name);
             System.out.println("Product category: " + productDetails.category);
             System.out.println("Product price: " + productDetails.price);
             System.out.println("Product amount: " + productDetails.amount); 
             System.out.println();
        }
    }
    
    public void showProduct() throws IOException{  
        
        int productId;
        System.out.println("Enter product ID: ");
        productId = scanner.nextInt();
        ProductList productList = productFileManagement.getProducts();
        
        for(int i=0;i<productList.product.size();i++){
            ProductDetails productDetails = productList.product.get(i);   
            if(productId==productDetails.id){
                System.out.println("Product ID: " + productDetails.id);
                System.out.println("Product name: " + productDetails.name);
                System.out.println("Product category: " + productDetails.category);
                System.out.println("Product price: " + productDetails.price);
                System.out.println("Product amount: " + productDetails.amount);
            } 
        }
    }
    
    public void deleteProduct() throws IOException{
        
        int productID;
        System.out.println("Enter product ID");
        productID = scanner.nextInt();
        ProductList productList = productFileManagement.getProducts(); 
        
        for(int i=0;i<productList.product.size();i++){
            ProductDetails prodDetails = productList.product.get(i);
            if(productID == prodDetails.id){
                
                productList.product.remove(i);
                System.out.println("Product with ID:" +  prodDetails.id +  " - " + prodDetails.name + " " + "was removed from the list" + "\n");
            } 
        }
        productFileManagement.replaceProducts(productList); 
    }
    
    public void showUpdatedProducts(){
        System.out.println("After a deletion, you have following products: ");       
    }
    
    public ProductResult shoppingBasket() throws IOException{
        
        String productId = "";
        String productAmount = "";
        List<ProductDetails> rows = new ArrayList<>(); 
        
        ProductList productList = productFileManagement.getProducts();

        System.out.println("Please provide details of buying product. Click q if you want to finish:");
        while(true){
            System.out.println("ID:");
            productId = scanner.nextLine();
            
            if(productId.equals("q")){
                System.out.println("Thank you");
                break;
            }
            System.out.println("Amount:");
            productAmount = scanner.nextLine();
        
            for(int i=0;i<productList.product.size();i++){
                ProductDetails productDetails = productList.product.get(i);  

                if(Integer.parseInt(productId)==productDetails.id && Integer.parseInt(productAmount)<=productDetails.amount){  
                    ProductDetails product = new ProductDetails();
                    product.id = productDetails.id;
                    product.name = productDetails.name;
                    product.category = productDetails.category;
                    product.price = productDetails.price;
                    product.amount = Integer.parseInt(productAmount);
                    
                    rows.add(product);                 
                    break;
                }
                else if(Integer.parseInt(productId)==productDetails.id && Integer.parseInt(productAmount) > productDetails.amount){
                    System.out.println("Sorry, requested amount is unavailable");
                    return null;
                }
            }
        }
        
        System.out.println();
        System.out.println("Your shopping:  ");
        System.out.println();
        System.out.println("ID   Name   Category   Amount   Price"); 
            
        double productTotalSum =0;
        double productSum =0;
        double productLineAmount;
        double productLinePrice;
        int productCount = 0;
        double productSale ;
        ProductResult productResult = new ProductResult();

        for (int i=1; i<=rows.size(); i++){
            
            ProductDetails productDetails = rows.get(i-1);
            System.out.println(productDetails.id +  "   " + productDetails.name + "   " + productDetails.category + "     " + productDetails.amount + "      " + productDetails.price);

            productSum = (productDetails.price * productDetails.amount) ;
            productTotalSum += productSum;  
            productCount  += productDetails.amount;
        }
        
        productResult.productCount = productCount;
        productResult.extraList = rows;
        productResult.productTotalPrice = productTotalSum;
        System.out.println("Product count: " + productCount);
        System.out.println("Total price is: " + productTotalSum);        
        return productResult; 
    }
    
    public void shoppingBasketSaleForAll (ProductResult productResult){
        int productCount = 0;
        double productSum = 0;
        System.out.println();
        System.out.println("Today we offer 30% less from the bill");
        /*while(productCount < productResult.extraList.size())
        {
         ProductDetails prodDet = productResult.extraList.get(productCount);
         productCount++; */
         
        productResult.productTotalPrice = (productResult.productTotalPrice * 0.7);
        System.out.println("Price after discount: " + productResult.productTotalPrice); 
                 
        /*for(int i=0;i<productList.size();i++)
        {
            ProductDetails product = productList.get(i);
            
            for(int j=1;j<=product.amount;j++)
            {
                if(j%2==0)
                {
                    productSumMax += 0.7 * product.price ;
                }
                else
                {
                    productSumMax += product.price;
                }
            }   
        } */
        
    }
    
    public void shoppingBasketSaleForCheaperProduct (ProductResult productResult){
        double minPrice=999999999;
        double maxPrice=0;
        double productSum=0;
        double productSumMin=0;
        double productSumMax=0;
        double productDiscount;
        System.out.println();
        System.out.println("Today we offer 30% discount for cheaper product");
        System.out.println();
        
        for(int i=0;i<productResult.extraList.size();i++){
            
            ProductDetails productDetails = productResult.extraList.get(i);
            
            if(productDetails.price<minPrice){
                minPrice=productDetails.price;
            }   
            if(productDetails.price>maxPrice){
                maxPrice=productDetails.price;
            }      
        }
        
        for(int i=0;i<productResult.extraList.size();i++){
            
            ProductDetails productDetails = productResult.extraList.get(i);
            productSum = productDetails.price;
            
            if(productDetails.price == minPrice){
                productSumMax += productDetails.price * 0.7 ;
                productSum = productSumMax + (productResult.productTotalPrice - minPrice);
                System.out.println("Sale includes: " + " " + productDetails.id + " " + productDetails.name + " " + productDetails.category);
                System.out.println("Total price after discount: " + productSum);
                break;
            }
            else{
                productSumMax += productDetails.price ;
                System.out.println("No chaeper product");
                System.out.println("Total price is: "+ productSumMax);
            }
        }    
    }
    
    public void shoppingBasketSaleNextProduct (ProductResult productResult){
        
        List<ProductDetails>nextProduct = new ArrayList<>();
        double productPrice;
        double percent = 0.9;
        double totalPrice=0;
        System.out.println();
        System.out.println("Today we offer -10% discount for next product");
        System.out.println("1.product -10%");
        System.out.println("2.product -20%");
        System.out.println("3.product -30%");
        System.out.println("4.product -40%");
        System.out.println();
        
        System.out.println("Your shopping: ");
        System.out.println();
        System.out.println("Name    Amount    Price(1)");
        
        for(int i=0;i<productResult.extraList.size();i++){
            
           ProductDetails productDetails = productResult.extraList.get(i);
           productPrice = productDetails.price * percent * productDetails.amount;
           percent = percent -0.1;
           totalPrice+=productPrice;
           System.out.printf(productDetails.name + "     " + productDetails.amount + "    " + "%.2f%n" , productPrice);
        }
        
        System.out.println("Total price after discoubt: " + totalPrice);
    }
}




    

