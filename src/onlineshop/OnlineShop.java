
package onlineshop;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;


public class OnlineShop {

    public static void main(String[] args) throws IOException{
        
        ShopController controller = new ShopController();

        while(true){
            System.out.println("Please choose an operation: ");
            System.out.println("1) Add products" );
            System.out.println("2) Show all products");
            System.out.println("3) Show product(ID) " );
            System.out.println("4) Delete product (ID) ");
            System.out.println("5) Buy products (ID,Amount) ");
            System.out.println();
        
            Scanner scanner = new Scanner(System.in);
            int digit = scanner.nextInt();
            
            switch(digit){
              case 1:
                  controller.addProduct();
                  break;

              case 2:
                  controller.showProducts();
                  break;

              case 3:
                  controller.showProduct();
                  break;

              case 4:
                  controller.deleteProduct();
                  break;

              case 5:              
                  ProductResult productResult = controller.shoppingBasket();
                  controller.shoppingBasketSaleForAll(productResult);
                  //controller.shoppingBasketSaleForCheaperProduct(productResult);
                  //controller.shoppingBasketSaleNextProduct(productResult);
                  break;                  
            
              default:
                  System.out.println("Incorrect number");      
            }
                        
        }
    }
}

