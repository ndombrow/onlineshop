
package onlineshop;


public class ProductDetails {
    public int id;
    public String name;
    public String category;
    public double price;
    public int amount;
}
