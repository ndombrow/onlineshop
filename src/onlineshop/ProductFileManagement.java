package onlineshop;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;


public class ProductFileManagement{
    private static String FilePath = "src\\onlineshop\\products.txt";
    private Scanner scanner = new Scanner(System.in);
    private ProductList productList; 
    
    public ProductFileManagement() throws IOException{
        productList = new ProductList();
    }
    
    public void addProduct(ProductDetails productDetails) throws IOException{
        FileWriter fileWriter = new FileWriter(FilePath, true);

        fileWriter.write(productDetails.id + "," + productDetails.name
        + "," + productDetails.category + "," + productDetails.price + "," + productDetails.amount + "\n");
        fileWriter.close();
    }
    
    /*public void deleteProduct() throws FileNotFoundException, IOException //dokonczyc
    {
        FileReader fileReader = new FileReader(FilePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        bufferedReader.close();
    } */
    
    public ProductList getProducts() throws FileNotFoundException, IOException{
        FileReader fileReader = new FileReader(FilePath);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String fileLine;
        ProductList list = new ProductList();
        while(true){
            fileLine = bufferedReader.readLine();
            if(fileLine==null){
                break;
            }
            
            String productSplit [] = fileLine.split(",");
            String id = productSplit [0];
            String name = productSplit [1];
            String category = productSplit [2];
            String price = productSplit [3]; 
            String amount = productSplit [4];

            ProductDetails productDetails = new ProductDetails();

            productDetails.id = Integer.parseInt(id);
            productDetails.name = name;
            productDetails.category = category;
            productDetails.price = Double.parseDouble(price);
            productDetails.amount = Integer.parseInt(amount); 

            list.product.add(productDetails);        
        }
        return list;
    }
    
    public void replaceProducts(ProductList list) throws FileNotFoundException, IOException{
        FileWriter fileWriter = new FileWriter(FilePath);
        fileWriter.flush();
        
        for(int i=0;i<list.product.size();i++){
            ProductDetails productDetails = list.product.get(i);
            
            fileWriter.write(productDetails.id + "," + productDetails.name
            + "," + productDetails.category + "," + productDetails.price + "," + productDetails.amount + "\n");            
        }
        fileWriter.close();
    }
    
    /*public void buyProducts(ProductList list)
    {
        int productId = 0;
        int productAmount=0;
        
        for(int i=0;i<list.product.size();i++)
        {
            ProductDetails productDetails = list.product.get(i);
            if(productId==productDetails.id)
            {
               System.out.println("You choose product ID: " + productId);
            }
            if(productAmount<=productDetails.amount)
            {
                System.out.println("Requested amount is available");
            }
            else
            {
               System.out.println("The amount is temporarily unavailable");
            }
               System.out.println("Your shopping: ");
               System.out.println("ID   Name   Amount   Price");
               System.out.println(productId + " " + productDetails.name + " " + productAmount + " " + productDetails.price);
            }
        } */
}
 
